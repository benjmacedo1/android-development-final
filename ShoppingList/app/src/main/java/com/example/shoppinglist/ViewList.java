package com.example.shoppinglist;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.Random;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

public class ViewList extends AppCompatActivity {

    //need to extract from database
    int[] IMAGES = {R.drawable.ic_launcher_background,R.drawable.ic_launcher_background,R.drawable.ic_launcher_background,R.drawable.ic_launcher_background,
            R.drawable.ic_launcher_background, R.drawable.ic_launcher_background,R.drawable.ic_launcher_background,R.drawable.ic_launcher_background,R.drawable.ic_launcher_background,
            R.drawable.ic_launcher_background};
    String[] itemNames = {"","","","","","","","","","",""};
    String[] itemQuantity={"","","","","","","","","","",""};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent mainPass = getIntent();
        Bundle extras = mainPass.getExtras();


        //myRowItems = new ArrayList<RowItem>();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "List has been updated and saved", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        populateLists();

        //list view code
        ListView listView =(ListView)findViewById(R.id.viewListContent);
        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);

        if(extras != null){
            EditText list_name=(EditText)findViewById(R.id.list_Name_EditView);
            EditText list_desc=(EditText)findViewById(R.id.description_EditView);
            list_name.setText((String) mainPass.getStringExtra("name"));
            list_desc.setText((String) mainPass.getStringExtra("desc"));

        }


    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return IMAGES.length;
        }

        @Override
        public Object getItem(int position) {

            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.view_list_items,null);

            ImageView item_image =(ImageView) convertView.findViewById(R.id.item_image);
            TextView item_name=(TextView)convertView.findViewById(R.id.item_name);
            EditText item_quantity=(EditText)convertView.findViewById(R.id.item_quantity);

            item_image.setImageResource(IMAGES[position]);
            item_name.setText(itemNames[position]);
            item_quantity.setText(itemQuantity[position]);
            return convertView;
        }
    }


    //read the database and retrieve lists
    public void populateLists(){
        Random rand = new Random();
        //for now just populate it for testing purposes
        int numEntries = 10;
        for(int i =0 ; i < numEntries; i++){

            itemNames[i] = "Item Name "+ i;
            itemQuantity[i] = Integer.toString(rand.nextInt(10));
            //IMAGES[i] = R.drawable.ic_launcher_background;
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

