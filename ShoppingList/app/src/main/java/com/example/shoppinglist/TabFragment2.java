package com.example.shoppinglist;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabFragment2 extends Fragment {




    public TabFragment2() {
        // Required empty public constructor
    }

    private String[] listName = {"","","","","","","","","",""};
    private boolean[] isSelected = {false, false, false, false, false, false, false, false, false, false};
    private String[] quantList = {"", "", "", "", "", "", "", "", "", ""};


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_fragment1, container, false);
        ListView listView = view.findViewById(R.id.listViewFrag);
        populateLists();

        CustomAdapter customAdapter = new CustomAdapter(getActivity(), listName, quantList, isSelected);
        listView.setAdapter(customAdapter);
        return view;
    }

    private void populateLists() {
        //for now just populate it for testing purposes
        int numEntries = 10;
        for (int i = 0; i < numEntries; i++) {

            listName[i] = "Vegetable " + i;
            quantList[i] = "0";


        }
    }
}
