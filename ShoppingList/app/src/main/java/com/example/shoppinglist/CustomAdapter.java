package com.example.shoppinglist;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

public class CustomAdapter extends BaseAdapter {

    private Context context;

    String[] listGroceries;
    String[] customQuant;
    boolean[] customIsSelected;
    CustomAdapter(Context context, String[] list, String[] quant, boolean[] selected) {
        this.listGroceries = list;
        this.context = context;
        this.customQuant = quant;
        this.customIsSelected = selected;
    }
    @Override
    public int getViewTypeCount() {
        return getCount();
    }
    @Override
    public int getCount() {
        return listGroceries.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    @Override
    public int getItemViewType(int position) {

        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder(); LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.new_list_template_layout, null, true);

            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
            holder.groceryItemTextView = (TextView) convertView.findViewById(R.id.new_list_items_TextView);
            holder.groceryQuantEditView = (EditText) convertView.findViewById(R.id.item_quantity_EditView);

            convertView.setTag(holder);
        }else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder)convertView.getTag();
        }


        holder.groceryItemTextView.setText(listGroceries[position]);
        holder.groceryQuantEditView.setText(String.format(Locale.getDefault(),"%s", customQuant[position]));
        holder.checkBox.setChecked(customIsSelected[position]);


        holder.checkBox.setTag(R.integer.btnplusview, convertView);
        holder.checkBox.setTag(position);

        //set tag for the editTextBox
        holder.groceryQuantEditView.setTag(R.integer.btnplusview, convertView);
        holder.groceryQuantEditView.setTag(position);
        //listGroceries[position]= holder.groceryQuantEditView.getText().toString();

        //when the text for item quantity gets changed the value will be updated in the list
        holder.groceryQuantEditView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {




                //get the view of the text that changed
                View tempView = (View) holder.groceryQuantEditView.getTag(R.integer.btnplusview);

                //get the corresponding editText
                EditText tempEdit = (EditText) tempView.findViewById(R.id.item_quantity_EditView);
                String quantity = tempEdit.getText().toString();

                //update the list (database)
                if(!quantity.isEmpty()){
                    System.out.println("Quantity: " +quantity);

                    Integer pos = (Integer) holder.groceryQuantEditView.getTag(); //I was able to fix it. P.S.
                    if(pos != null){
                        System.out.println("Position: "+pos);

                        customQuant[pos] = quantity;
                    }



                    //tempView.setText(quantity);
                }


            }
        });

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View tempView = (View) holder.checkBox.getTag(R.integer.btnplusview);

                TextView tv = (TextView) tempView.findViewById(R.id.new_list_items_TextView);

                Integer pos = (Integer) holder.checkBox.getTag();

                //set the quantity


                if (!customIsSelected[pos]) {
                    customIsSelected[pos] = true;
                } else {
                    customIsSelected[pos] = false;
                }
            }
        });
        return convertView;
    }

    private class ViewHolder {

        protected CheckBox checkBox;
        private TextView groceryItemTextView;
        private EditText groceryQuantEditView;

    }
}

